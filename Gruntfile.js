module.exports = function(grunt) {

    /*
        Grunt installation:
        -------------------
            npm install -g grunt-cli
            npm install -g grunt-init
            npm init (creates a `package.json` file)

        Project Dependencies:
        ---------------------
            npm install grunt-contrib-clean --save-dev
            npm install grunt-contrib-compass --save-dev
            npm install grunt-contrib-cssmin --save-dev
            npm install grunt-contrib-jshint --save-dev
            npm install grunt-contrib-uglify --save-dev
            npm install grunt-contrib-requirejs --save-dev
    */

    // Project configuration.
    grunt.initConfig({

        // Store your Package file so you can reference its specific data
        // whenever necessary. If the project has already been setup with
        // Grunt, then running 'npm install' on the CMD line from the project
        // root folder, (where the package.json and Gruntfile.js live) will
        // install all the plugin dependencies in the package.json file.
        pkg: grunt.file.readJSON('package.json'),


        // Grunt task definition to clean the build folder
        clean: {
            build: {
                src: ["assets/build"]
            }
        },

        compass: {
            build: {
                options: {
                    config: '.config.rb'
                }
            }
        },

        // Grunt task definition to minify the compiled single CSS file output by Sass
        cssmin: {
            combine: {
                options: {
                    banner: '/*! <%= pkg.name %> v<%= pkg.version %> :: <%= grunt.template.today("dd-mm-yyyy") %> :: Production CSS */'
                },
                files: {
                    'assets/build/enhanced.min.css': ['assets/css/enhanced.css'],
                    'assets/build/core.min.css': ['assets/css/core.css']
                }
            }
        },

        // Grunt task definition to lint all JS modules
        jshint: {
            files: ['assets/js/modules/**/*.js'],
            options: {
                // options here to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },

        // Compress the bootstrap javascript
        uglify: {
            build: {
                files: {
                    'assets/build/bootstrap.min.js': ['assets/js/bootstrap.js']
                }
            }
        },

        // Grunt task definition to optimise RequireJS and all modules and dependencies
        requirejs: {
            buildjs: {
                options: {

                    appDir: "assets",

                    baseUrl: "js/lib",

                    paths: {
                        app: '../app'
                    },

                    dir: "assets/build",

                    fileExclusionRegExp: /^css$|sass|^tools|^bootstrap/,

                    keepBuildDir: true,

                    preserveLicenseComments: false,

                    //optimize: "none",

                    modules: [
                        {
                            name: "../common",
                            include: [
                                "jquery"
                            ]
                        },
                        {
                            name: "../index",
                            include: [
                                "app/module-a",
                                "app/module-b",
                                "app/module-c",
                                "app/module-d",
                            ],
                            exclude: ["../common"]

                        },
                        {
                            name: "../page-1",
                            include: ["app/main1"],
                            exclude: ["../common"]
                        },
                        {
                            name: "../page-2",
                            include: ["app/main2"],
                            exclude: ["../common"]
                        }
                    ]
                }
            }
        }
    });


    // Load NPM Tasks
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-requirejs');


    // Default task
    // Simply run using 'grunt' at the command line.
    grunt.registerTask("default", ['jshint', 'clean', 'requirejs', 'compass', 'cssmin', 'uglify']);

    // Add further registered tasks as required, e.g.
    grunt.registerTask("require", ['jshint', 'requirejs']);

    // You can run any individual task by using 'grunt [taskname]'
    // e.g. 'grunt clean' or 'grunt requirejs' etc.
};