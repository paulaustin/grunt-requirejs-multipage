# requirejs/example-multipage

This project shows how to set up a multi-page requirejs-based project that has
the following goals:

* Each page uses a mix of common and page-specific modules.
* All pages share the same requirejs config.
* After an optimization build, the common items should be in a shared common
layer, and the page-specific modules should be in a page-specific layer.
* The HTML page should not have to be changed after doing the build.

## Grunt ##

Having cloned the Git repository run `npm install` from the command line to install grunt and the grunt plugins listed in the `package.json` file. This will create a `node_modules` folder.

You can now run `grunt` from the command line. `grunt` is the default task and runs the complete build process as defined in `Gruntfile.js`:

`default` task:

* **jshint**: lint all the scripts in the app folder.
* **clean**: delete the build folder ready for the fresh build.
* **requirejs**: run the r.js optimiser to build the production scripts.
* **compass**: compile the Sass into the css folder.
* **cssmin**: produce compressed versions of the CSS files in the build folder.
* **uglify**: produce a compressed version of the bootstrap script in the build folder.

`require` task:

* **jshint**: lint all the scripts in the app folder.
* **requirejs**: run the r.js optimiser to build the production scripts.

You can also run any individual task by running `grunt [taskname]` e.g. `grunt compass`.  

## Project layout

This project has the following folder structure:

* **assets/sass**: The source Sass mixins and partials files.
* **assets/css**: The CSS files compiled from the Sass.
* **assets/js**: All source JavaScript files. 
* **assets/build**: The folder created by the build process which contains the production ready scripts and stylesheets.

Under **assets/js** we have:

* **app**: the directory to store app-specific modules.
* **lib**: the directory to hold third party modules, like jQuery.
* **bootstrap.js**: the bootstrap code which tests the browser before dynamically loading the script(s).
* **common.js**: contains the RequireJS config and it will be the build target for the set of common modules.

* **html**
	* **index.html**: Home page of the app.
	* **page-1.html**: page 1 of the app.
	* **page-2.html**: page 2 of the app.

* **js**
	* **index.js**: used for the data-main for index.html. Loads the common
    module, then loads _app/module-a_, _app/module-b_, _app/module-c_, _app/module-d_ the main module for the home page.
    * **page1.js**: used for the data-main for page1.html. Loads the common
    module, then loads _app/main1_, the main module for page 1.
    * **page2.js**: used for the data-main for page2.html. Loads the common
    module, then loads _app/main2_, the main module for page 2.

To optimize, run:

    grunt

That build command creates an optimized version of the project in a
**assets/build** directory. The **build/js/common.js** file will contain all the common
modules. **build/js/index.js** will contain the index-specific modules, **build/js/page-1.js** will contain the page1-specific modules, **build/js/page-2.js** will contain the page-2-specific modules.

## Building up the common layer

As you do builds and see in the build output that each page is including the
same module, add it to common's "include" array in **Gruntfile.js**.

It is better to add these common modules to the **Gruntfile.js** config
instead of doing a require([]) call for them in **js/common.js**. Modules that
are not explicitly required at runtime are not executed when added to common.js
via the include build option. So by using **tools/build.js**, you can include
common modules that may be in 2-3 pages but not all pages. For pages that do
not need a particular common module, it will not be executed. If you put in a
require() call for it in **js/common.js**, then it will always be executed.

## More info

For more information on the optimizer:
http://requirejs.org/docs/optimization.html

For more information on using requirejs:
http://requirejs.org/docs/api.html
