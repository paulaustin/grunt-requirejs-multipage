//Load common code that includes config, then load the app logic for this page.
require(["./common"], function (common) {
    require([
        "app/module-a",
        "app/module-b",
        "app/module-c",
        "app/module-d"
    ]);
});