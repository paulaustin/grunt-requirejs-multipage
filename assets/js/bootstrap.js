//
// Boot strap the application only if the browser passes the HTML5 test
// i.e. "cut's the mustard".
//
// http://responsivenews.co.uk/post/18948466399/cutting-the-mustard
//
// Modernizr is loaded at this point, so these tests could be Modernizr
// tests instead of the BBC Cutting The Mustard test
//
(function() {
    if ( 'querySelector' in document && 'addEventListener' in window ) {
        
        console.log(Modernizr);

        // Add common script
        var script = document.createElement("script"),
            pageScript = document.body.getAttribute("data-script");
        
        script.async = true;
        script.src = "assets/build/js/lib/require.js";

        // Optionally add page specific script
        if ( pageScript !== null ) {
            script.setAttribute("data-main", "assets/build/js/" + pageScript);
        }

        document.body.appendChild(script);
    }
})();